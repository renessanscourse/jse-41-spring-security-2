package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import ru.ovechkin.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class Session extends AbstractEntity {

    @Nullable
    @JsonFilter("idFilter")
    private User user;

    private String signature;

    private long creationTime = new Date().getTime();

    @NotNull
    private Role role;

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User user) {
        this.user = user;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setSignature(@Nullable String signature) {
        this.signature = signature;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

}