# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allProjects**](DefaultApi.md#allProjects) | **GET** /rest/projects/all | 
[**create**](DefaultApi.md#create) | **PUT** /rest/projects/create | 
[**create_0**](DefaultApi.md#create_0) | **PUT** /rest/tasks/create | 
[**edit**](DefaultApi.md#edit) | **POST** /rest/projects/edit | 
[**edit_0**](DefaultApi.md#edit_0) | **POST** /rest/tasks/edit | 
[**remove**](DefaultApi.md#remove) | **DELETE** /rest/projects/remove | 
[**remove_0**](DefaultApi.md#remove_0) | **DELETE** /rest/tasks/remove | 
[**show**](DefaultApi.md#show) | **GET** /rest/tasks/{projectId} | 


<a name="allProjects"></a>
# **allProjects**
> List&lt;Project&gt; allProjects()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<Project> result = apiInstance.allProjects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#allProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="create"></a>
# **create**
> List&lt;Project&gt; create(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Project body = new Project(); // Project | 
try {
    List<Project> result = apiInstance.create(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Project**](Project.md)|  | [optional]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="create_0"></a>
# **create_0**
> List&lt;Task&gt; create_0(projectId, body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String projectId = "projectId_example"; // String | 
Task body = new Task(); // Task | 
try {
    List<Task> result = apiInstance.create_0(projectId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **body** | [**Task**](Task.md)|  | [optional]

### Return type

[**List&lt;Task&gt;**](Task.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="edit"></a>
# **edit**
> List&lt;Project&gt; edit(id, body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
Project body = new Project(); // Project | 
try {
    List<Project> result = apiInstance.edit(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |
 **body** | [**Project**](Project.md)|  | [optional]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="edit_0"></a>
# **edit_0**
> List&lt;Task&gt; edit_0(projectId, taskId, body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String projectId = "projectId_example"; // String | 
String taskId = "taskId_example"; // String | 
Task body = new Task(); // Task | 
try {
    List<Task> result = apiInstance.edit_0(projectId, taskId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **taskId** | **String**|  |
 **body** | [**Task**](Task.md)|  | [optional]

### Return type

[**List&lt;Task&gt;**](Task.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="remove"></a>
# **remove**
> List&lt;Project&gt; remove(projectId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String projectId = "projectId_example"; // String | 
try {
    List<Project> result = apiInstance.remove(projectId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#remove");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="remove_0"></a>
# **remove_0**
> List&lt;Task&gt; remove_0(taskId, projectId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String taskId = "taskId_example"; // String | 
String projectId = "projectId_example"; // String | 
try {
    List<Task> result = apiInstance.remove_0(taskId, projectId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#remove_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **String**|  |
 **projectId** | **String**|  |

### Return type

[**List&lt;Task&gt;**](Task.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="show"></a>
# **show**
> List&lt;Task&gt; show(projectId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String projectId = "projectId_example"; // String | 
try {
    List<Task> result = apiInstance.show(projectId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#show");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**List&lt;Task&gt;**](Task.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

