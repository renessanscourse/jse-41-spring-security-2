
# Task

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**finishDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**creationTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



