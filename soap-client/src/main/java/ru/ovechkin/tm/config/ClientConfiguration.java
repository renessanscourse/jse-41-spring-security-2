package ru.ovechkin.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ovechkin.tm.soap.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.soap.endpoint.ProjectEndpointService;
import ru.ovechkin.tm.soap.endpoint.TaskEndpoint;
import ru.ovechkin.tm.soap.endpoint.TaskEndpointService;

@Configuration
@ComponentScan("ru.ovechkin.tm")
public class ClientConfiguration {

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean TaskEndpoint taskEndpoint(
            @NotNull @Autowired TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}
