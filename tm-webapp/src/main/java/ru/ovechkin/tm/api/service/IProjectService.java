package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.CustomUser;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAllUserProjects(CustomUser user);

    @Transactional
    void save(@Nullable Project project, @Nullable CustomUser user);

    @Transactional
    void removeById(@Nullable String projectId, @Nullable CustomUser user);

    Project findById(@Nullable String projectId);

    @Transactional
    void updateById(@Nullable String id, @Nullable Project project, @Nullable CustomUser user);
}