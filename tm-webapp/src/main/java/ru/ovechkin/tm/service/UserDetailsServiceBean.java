package ru.ovechkin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.dto.CustomUser;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;
import ru.ovechkin.tm.repository.UserRepository;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService, IUserService {

    @Autowired
    private UserRepository userRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(user.getRole().name())
                .build())
                .withUserId(user.getId());
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void registry(@Nullable final User user) {
        if (user == null) throw new UserEmptyException();
        user.setRole(Role.USER);
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        userRepository.save(user);
    }

}