package ru.ovechkin.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import ru.ovechkin.tm.dto.CustomUser;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;

public class UserUtil {

    static public CustomUser getUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException();
        return (CustomUser) principal;
    }
}
