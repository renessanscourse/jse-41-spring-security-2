package ru.ovechkin.tm.exeption.unknown;

public class TaskUnknownException extends RuntimeException {

    public TaskUnknownException() {
        super("Error! This Task does not exist.");
    }

    public TaskUnknownException(final String name) {
        super("Error! This Task [" + name + "] does not exist.");
    }

}
