package ru.ovechkin.tm.exeption.user;

public class AlreadyExistLoginException extends RuntimeException {

    public AlreadyExistLoginException(final String login) {
        super("Error! This login: [" + login + "] already exists...");
    }

}