package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.dto.CustomUser;
import ru.ovechkin.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/{projectId}")
    public String show(
            @PathVariable("projectId") final String projectId,
            @AuthenticationPrincipal CustomUser user,
            Model model
    ) {
        model.addAttribute("tasks", taskService.findAll(projectId, user));
        return "tasks/all";
    }

    @GetMapping("/createForm")
    public String getCreateTaskForm(@RequestParam("projectId") String projectId, Model model) {
        model.addAttribute("task", new Task());
        model.addAttribute("projectId", projectId);
        return "tasks/create";
    }

    @PostMapping("/create")
    public String create(
            @RequestParam("projectId") String projectId,
            @ModelAttribute("task") Task task,
            @AuthenticationPrincipal CustomUser user
    ) {
        task.setProject(projectService.findById(projectId));
        taskService.save(task, user);
        return ("redirect:/tasks/" + projectId);
    }

    @GetMapping("/remove")
    public String remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId,
            @AuthenticationPrincipal CustomUser user
    ) {
        taskService.removeById(taskId, user);
        return ("redirect:/tasks/" + projectId);
    }

    @GetMapping("/editForm/{id}")
    public String getEditForm(@PathVariable("id") final String taskId, Model model) {
        model.addAttribute("task", taskService.findById(taskId));
        return "/tasks/edit";
    }

    @PostMapping("/edit")
    public String edit(
            @ModelAttribute("task") final Task task,
            @RequestParam("projectId") final String projectId,
            @RequestParam("taskId") final String taskId,
            @AuthenticationPrincipal CustomUser user
    ) {
        taskService.updateById(taskId, task, user);
        return ("redirect:/tasks/" + projectId);
    }

}