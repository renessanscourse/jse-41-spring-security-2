<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>LOGIN PAGE</title>
    </head>

    <header class="header"></header>

    <body style="text-align: center">

        <h1>LOGIN WITH USERNAME AND PASSWORD</h1>

        <form name="f" method="POST" action="/login">
            <div style="margin-top: 10px">
                <a>Username:</a>
            </div>
            <div style="margin-top: 10px">
                <label>
                    <input type="text" name="username" value=''>
                </label>
            </div>
            <div style="margin-top: 10px">
                <a>Password:</a>
            </div>
            <div style="margin-top: 10px">
                <label>
                    <input type="password" name="password">
                </label>
            </div>
            <div><br></div>
            <div>
                <input type="submit" name="submit" value="login" class="customButton">
            </div>
        </form>

    </body>

</html>
