package ru.ovechkin.tm.controller;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ovechkin.tm.config.WebMvcConfig;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

//    @Autowired
//    private ProjectController projectController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testAllProjects() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/all"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("all"));
    }

    @Test
    public void testGetCreateProjectForm() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/createForm"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("create"));
    }

    @Test
    public void testCreate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("all"));
    }

    @Test
    public void testRemove() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/remove"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("all"));
    }

    @Test
    public void testGetEditForm() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/{id}"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("edit"));
    }

    @Test
    public void testEdit() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects/edit"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("all"));
    }

}