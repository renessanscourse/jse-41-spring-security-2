package ru.ovechkin.tm.service;

import junit.framework.TestCase;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.repository.ProjectRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectServiceTest {

    public void testFindAllUserProjects() {

    }

    public void testSave() {
    }

    public void testRemoveById() {
    }

    public void testFindById() {
    }

    public void testUpdateById() {
    }
}