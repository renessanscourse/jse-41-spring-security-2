package ru.ovechkin.tm.repository;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Task;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class TaskRepositoryTest {

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void testDeleteTaskById() {
    }

    @Test
    public void testSave() {
        final Task task = new Task();
        task.setName("TEST_SAVE");
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void testFindById() {
        final Task task = new Task();
        task.setName("TEST_FIND_ID");
        taskRepository.save(task);
        assertEquals(task.toString(), taskRepository.findById(task.getId()).get().toString());
    }

    @Test
    public void testDelete() {
        final Task task = new Task();
        task.setName("TEST_DELETE");
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
        taskRepository.delete(task);
        assertEquals(Optional.empty(), taskRepository.findById(task.getId()));
    }

    @Test
    public void testUpdate() {
        final Task task = new Task();
        task.setName("TEST_TO_CHANGE");
        taskRepository.save(task);
        assertEquals(task.toString(), taskRepository.findById(task.getId()).get().toString());
        task.setName("TEST_CHANGED");
        taskRepository.save(task);
        assertEquals(task.toString(), taskRepository.findById(task.getId()).get().toString());
    }

}